package pwc.generator.pdf.service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;

import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.html.WebColors;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfWriter;

import org.javatuples.Pair;
import org.springframework.stereotype.Service;

public class BpaGenerator {

    private PdfPTable documentTitleTable = new PdfPTable(1);
    private PdfPTable documentHeaderTable = new PdfPTable(3);
    private PdfPTable firstBodyTable = new PdfPTable(3);
    private PdfPTable bodySeparatorTable = new PdfPTable(1);
    private PdfPTable secondBodyTable = new PdfPTable(3);
    private PdfPTable documentFooterTable = new PdfPTable(3);
    private PdfPTable noteTable = new PdfPTable(1);

    public class HeaderTable extends PdfPageEventHelper {
        protected PdfPTable documentTitleTable;
        protected PdfPTable documentHeaderTable;
        protected float tableHeight;

        public HeaderTable(String documentTitle) {
            documentTitleTable = new PdfPTable(1);
            documentTitleTable.setWidthPercentage(100);

            documentHeaderTable = new PdfPTable(3);
            documentHeaderTable.setWidthPercentage(100);
        }
    }

    public ByteArrayInputStream generateBpa() throws DocumentException, IOException {
        Document document = new Document(PageSize.A4);

        System.out.println(document.bottomMargin());
        System.out.println(document.leftMargin());
        System.out.println(document.rightMargin());
        System.out.println(document.topMargin());

        document.setMargins(36, 36, 36, 0);
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        PdfWriter.getInstance(document, output);

        document.open();

        String defaultFont = "src/main/resources/static/font/Roboto/Roboto-Regular.ttf";
        FontFactory.register(defaultFont, "defaultFont");
        BaseFont baseDefaultFont = BaseFont.createFont(defaultFont, BaseFont.IDENTITY_H, BaseFont.EMBEDDED);

        // BaseColor
        BaseColor documentTitleBackColor = WebColors.getRGBColor("E03021");
        BaseColor documentHeaderFontColor = WebColors.getRGBColor("F16333");

        // Fonts
        Font documentTitleFont = new Font(baseDefaultFont, 12, Font.BOLD, BaseColor.WHITE);
        Font documentHeaderFont = new Font(baseDefaultFont, 18, Font.BOLD, documentHeaderFontColor);
        Font firstBodyFont1 = new Font(baseDefaultFont, 12, Font.NORMAL, BaseColor.BLACK);
        Font firstBodyFont2 = new Font(baseDefaultFont, 14, Font.BOLD, BaseColor.BLACK);
        Font secondBodyFont = new Font(baseDefaultFont, 12, Font.BOLD, BaseColor.BLACK);
        Font normalNoteFont = new Font(baseDefaultFont, 9, Font.NORMAL, BaseColor.BLACK);
        Font boldNoteFont = new Font(baseDefaultFont, 9, Font.BOLD, BaseColor.BLACK);
        Font italicNoteFont = new Font(baseDefaultFont, 9, Font.ITALIC, BaseColor.BLACK);

        // Document Title "BUKTI PENERIMAAN ASP"
        documentTitleTable.setWidthPercentage(100);
        Chunk documentTitleBackground = new Chunk("BUKTI PENERIMAAN ASP", documentTitleFont);
        documentTitleBackground.setBackground(documentTitleBackColor, 5, 3, 5, 5);
        Paragraph documentTitleText = new Paragraph(documentTitleBackground);
        documentTitleText.setAlignment(PdfPCell.ALIGN_RIGHT);
        documentTitleTable.addCell(getCell(documentTitleText, PdfPCell.ALIGN_RIGHT));

        document.add(documentTitleTable);

        /**
         * Document Header "ASP | PT. PRIMA WAHANA CARAKA"
         */
        documentHeaderTable.setSpacingBefore(20);
        documentHeaderTable.setWidthPercentage(100);
        documentHeaderTable.setWidths(new int[] { 4, 1, 10 });
        String pathHeaderAspLogo = "classpath:/static/icon/asp-logo.png";
        String pathHeaderSeparatorLogo = "classpath:/static/icon/header-separator.png";
        String headerAspName = "PT FINTEK INTEGRASI DIGITAL";

        // ASP LOGO
        Image headerAspLogo = Image.getInstance(pathHeaderAspLogo);
        // headerAspLogo.scaleAbsolute(50, 50);
        PdfPCell headerAspLogoCell = new PdfPCell(headerAspLogo);
        headerAspLogoCell.setBorder(Rectangle.NO_BORDER);
        headerAspLogoCell.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);

        documentHeaderTable.addCell(headerAspLogoCell);

        // HEADER LINE SEPARATOR
        Image headerSeparator = Image.getInstance(pathHeaderSeparatorLogo);
        PdfPCell headerSeparatorCell = new PdfPCell(headerSeparator);
        headerSeparatorCell.setBorder(Rectangle.NO_BORDER);
        headerSeparatorCell.setHorizontalAlignment(PdfPCell.ALIGN_MIDDLE);

        documentHeaderTable.addCell(headerSeparatorCell);

        // COMPANY NAME
        Chunk chunkHeaderAspName = new Chunk(headerAspName, documentHeaderFont);
        Paragraph textHeaderAspName = new Paragraph(chunkHeaderAspName);
        textHeaderAspName.setAlignment(PdfPCell.ALIGN_LEFT);
        PdfPCell headerAspNameCell = new PdfPCell(textHeaderAspName);
        headerAspNameCell.setBorder(Rectangle.NO_BORDER);
        headerAspNameCell.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        headerAspNameCell.setVerticalAlignment(PdfPCell.ALIGN_MIDDLE);

        documentHeaderTable.addCell(headerAspNameCell);

        document.add(documentHeaderTable);

        /**
         * Document First Body
         */
        firstBodyTable.setSpacingBefore(20);
        firstBodyTable.setWidthPercentage(100);
        firstBodyTable.setWidths(new int[] { 1, 5, 10 });
        String pathCalendarIcon = "classpath:/static/icon/calendar-vector.png";
        String pathWatchIcon = "classpath:/static/icon/watch-vector.png";

        String strTanggalPenerimaan = "Tanggal Penerimaan";
        String strNomorTransaksiPengiriman = "NOMOR TRANSAKSI PENGIRIMAN";
        String strJam = "Jam";
        String strKodePJAP = "KODE PENYEDIA JASA APLIKASI";

        String strReceiptDate = "12/05/2020";
        String strTransmissionTransactionNumber = "1234567812345869";
        String strTime = "12:00";
        String strAspCode = "12345";

        /**
         * One row -> Calendar Image, Tanggal Penerimaan, Nomor Transaksi Pengiriman
         */

        // Calendar Image
        Image calendarImage = Image.getInstance(pathCalendarIcon);
        PdfPCell calendarImageCell = new PdfPCell(calendarImage);
        calendarImageCell.setBorder(Rectangle.NO_BORDER);
        calendarImageCell.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        calendarImageCell.setVerticalAlignment(PdfPCell.ALIGN_MIDDLE);

        firstBodyTable.addCell(calendarImageCell);

        // Tanggal Penerimaan Text
        Chunk chunkTanggalPenerimaan = new Chunk(strTanggalPenerimaan, firstBodyFont1);
        Paragraph textTanggalPenerimaan = new Paragraph(chunkTanggalPenerimaan);
        Chunk chunkReceiptDate = new Chunk(strReceiptDate, firstBodyFont2);
        Paragraph textReceiptDate = new Paragraph(chunkReceiptDate);
        PdfPCell tanggalPenerimaanCell = new PdfPCell();
        tanggalPenerimaanCell.addElement(textTanggalPenerimaan);
        tanggalPenerimaanCell.addElement(textReceiptDate);
        tanggalPenerimaanCell.setBorder(Rectangle.NO_BORDER);
        tanggalPenerimaanCell.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);

        firstBodyTable.addCell(tanggalPenerimaanCell);

        // Nomor Transaksi Pengiriman
        Chunk chunkNomorTransaksiPengiriman = new Chunk(strNomorTransaksiPengiriman, firstBodyFont1);
        Paragraph textNomorTransaksiPengiriman = new Paragraph(chunkNomorTransaksiPengiriman);
        Chunk chunkTransmissionTransactionNumber = new Chunk(strTransmissionTransactionNumber, firstBodyFont2);
        Paragraph textTransmissionTransactionNumber = new Paragraph(chunkTransmissionTransactionNumber);
        PdfPCell nomorTransaksiPengirimanCell = new PdfPCell();
        nomorTransaksiPengirimanCell.addElement(textNomorTransaksiPengiriman);
        nomorTransaksiPengirimanCell.addElement(textTransmissionTransactionNumber);
        nomorTransaksiPengirimanCell.setBorder(Rectangle.NO_BORDER);
        nomorTransaksiPengirimanCell.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);

        firstBodyTable.addCell(nomorTransaksiPengirimanCell);

        // BREAK
        for (int i = 0; i < 3; i++) {
            PdfPCell paddingBetweenRow = new PdfPCell(new Phrase(" "));
            paddingBetweenRow.setPadding(5);
            paddingBetweenRow.setBorder(Rectangle.NO_BORDER);
            firstBodyTable.addCell(paddingBetweenRow);
        }

        /**
         * First Body
         *  One row -> Watch Image, Jam, Kode Penyedia Jasa Aplikasi
         */

        // Watch Image
        Image watchImage = Image.getInstance(pathWatchIcon);
        PdfPCell watchImageCell = new PdfPCell(watchImage);
        watchImageCell.setBorder(Rectangle.NO_BORDER);
        watchImageCell.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        watchImageCell.setVerticalAlignment(PdfPCell.ALIGN_MIDDLE);

        firstBodyTable.addCell(watchImageCell);

        // Jam Text
        Chunk chunkJam = new Chunk(strJam, firstBodyFont1);
        Paragraph textJam = new Paragraph(chunkJam);
        Chunk chunkTime = new Chunk(strTime, firstBodyFont2);
        Paragraph textTime = new Paragraph(chunkTime);
        PdfPCell jamCell = new PdfPCell();
        jamCell.addElement(textJam);
        jamCell.addElement(textTime);
        jamCell.setBorder(Rectangle.NO_BORDER);
        jamCell.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);

        firstBodyTable.addCell(jamCell);

        // Kode Penyedia Jasa Aplikasi Text
        Chunk chunkKodePJAP = new Chunk(strKodePJAP, firstBodyFont1);
        Paragraph textKodePJAP = new Paragraph(chunkKodePJAP);
        Chunk chunkAspCode = new Chunk(strAspCode, firstBodyFont2);
        Paragraph textAspCode = new Paragraph(chunkAspCode);
        PdfPCell kodePJAPCell = new PdfPCell();
        kodePJAPCell.addElement(textKodePJAP);
        kodePJAPCell.addElement(textAspCode);
        kodePJAPCell.setBorder(Rectangle.NO_BORDER);
        kodePJAPCell.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);

        firstBodyTable.addCell(kodePJAPCell);

        document.add(firstBodyTable);

        /**
         * Body Line Separator
         */
        bodySeparatorTable.setSpacingBefore(20);
        bodySeparatorTable.setWidthPercentage(100);
        String pathBodySeparator = "classpath:/static/icon/body-separator.png";

        Image bodySeparator = Image.getInstance(pathBodySeparator);
        PdfPCell bodySeparatorCell = new PdfPCell(bodySeparator);
        bodySeparatorCell.setBorder(Rectangle.NO_BORDER);
        bodySeparatorCell.setVerticalAlignment(PdfPCell.ALIGN_MIDDLE);

        bodySeparatorTable.addCell(bodySeparatorCell);

        document.add(bodySeparatorTable);

        /**
         * Document Second Body 
         */
        secondBodyTable.setSpacingBefore(20);
        secondBodyTable.setWidthPercentage(100);
        secondBodyTable.setWidths(new int[] {4, 1, 8});
        List<Pair<String, String>> secondBodyRows = new ArrayList<>();
        secondBodyRows.add(new Pair<>("NPWP", "31.400.108.2-121.100"));
        secondBodyRows.add(new Pair<>("JENIS PAJAK", "31.400.108.2-121.100"));
        secondBodyRows.add(new Pair<>("MASA / TAHUN PAJAK", "31.400.108.2-121.100"));
        secondBodyRows.add(new Pair<>("KODE PEMBETULAN", "31.400.108.2-121.100"));
        secondBodyRows.add(new Pair<>("FILE YANG DILAPOR", "31.400.108.2-121.100"));

        addRows(secondBodyRows, 1, firstBodyFont1, secondBodyTable);

        document.add(secondBodyTable);

        /**
         * Document Footer 
         * One row -> Halaman 1 dari 1, TERDAFTAR DAN DIAWASI OLEH DJP, DJP Barcode
         */
        documentFooterTable.setSpacingBefore(155);
        documentFooterTable.setWidthPercentage(100);
        documentFooterTable.setWidths(new int[] {6, 5, 1});
        String pathDjpBarcode = "classpath:/static/icon/djp-barcode.png";

        // Halaman 1 dari 1
        Chunk chunkHalaman = new Chunk("Halaman 1 dari 1", firstBodyFont1);
        Paragraph textHalaman = new Paragraph(chunkHalaman);
        PdfPCell halamanCell = new PdfPCell();
        halamanCell.addElement(textHalaman);
        halamanCell.setBorder(Rectangle.NO_BORDER);
        halamanCell.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        halamanCell.setVerticalAlignment(PdfPCell.ALIGN_BOTTOM);

        documentFooterTable.addCell(halamanCell);

        // TERDAFTAR DAN DIAWASI OLEH DJP
        Chunk chunkTerdaftar = new Chunk("TERDAFTAR DAN DIAWASI OLEH DJP", firstBodyFont1);
        Paragraph textTerdaftar = new Paragraph(chunkTerdaftar);
        PdfPCell terdaftarCell = new PdfPCell();
        terdaftarCell.addElement(textTerdaftar);
        terdaftarCell.setBorder(Rectangle.NO_BORDER);
        terdaftarCell.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
        terdaftarCell.setVerticalAlignment(PdfPCell.ALIGN_BOTTOM);

        documentFooterTable.addCell(terdaftarCell);

        // DJP BARCODE
        Image djpBarcodeImage = Image.getInstance(pathDjpBarcode);
        PdfPCell djpBarcodeCell = new PdfPCell(djpBarcodeImage);
        djpBarcodeCell.setBorder(Rectangle.NO_BORDER);
        djpBarcodeCell.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        djpBarcodeCell.setVerticalAlignment(PdfPCell.ALIGN_MIDDLE);

        documentFooterTable.addCell(djpBarcodeCell);
        
        document.add(documentFooterTable);

        /**
         * Catatan
         */
        noteTable.setSpacingBefore(5);
        noteTable.setWidthPercentage(113);
        Chunk chunkCatatan = new Chunk("Catatan:", boldNoteFont);
        Paragraph textCatatan = new Paragraph(chunkCatatan);

        Chunk chunkFirstNote = new Chunk("1. Bukti Penerimaan ASP (BPA) merupakan sebuah bukti lapor sementara yang menandakan bahwa pelaporan SPT Anda sudah berhasil diterima oleh Andalan Solusi Pajak dan sedang dalam proses disalurkan ke Direktorat Jenderal Pajak (DJP) untuk mendapatkan Bukti Penerimaan Elektronik (BPE).", normalNoteFont);
        Paragraph textFirstNote = new Paragraph(chunkFirstNote);
        Chunk chunkSecondNote = new Chunk("2. BPE adalah bukti pelaporan SPT yang sah, dan bukan BPA. Namun demikian tanggal lapor pada BPA Anda (tanggal file diterima lengkap oleh Andalan Solusi Pajak) akan sama pada tanggal lapor BPE Anda.", normalNoteFont);
        Paragraph textSecondNote = new Paragraph(chunkSecondNote);
        Chunk chunkThirdNote = new Chunk("3. Jika Anda belum mendapatkan BPE dua hari setelah Anda mendapatkan BPA, silakan hubungi id_andalansolusipajak@pwc.com", normalNoteFont);
        Paragraph textThirdNote = new Paragraph(chunkThirdNote);
        Chunk chunkFourthNote = new Chunk("Untuk kenyamanan Anda, sebaiknya lakukanlah e-Filing lebih awal sebelum tenggat pelaporan pajak.", italicNoteFont);
        Paragraph textFourthNote = new Paragraph(chunkFourthNote);

        PdfPCell noteCell = new PdfPCell();
        noteCell.addElement(textCatatan);
        noteCell.addElement(textFirstNote);
        noteCell.addElement(textSecondNote);
        noteCell.addElement(textThirdNote);
        noteCell.addElement(textFourthNote);
        noteCell.setPadding(20);
        noteCell.setPaddingBottom(50);

        noteCell.setBorder(Rectangle.NO_BORDER);

        noteCell.setBackgroundColor(new BaseColor(252,229,224));

        noteTable.addCell(noteCell);

        document.add(noteTable);

        document.close();

        return new ByteArrayInputStream(output.toByteArray());
    }
    

    public PdfPCell getCell(String text, int alignment) {
        PdfPCell cell = new PdfPCell(new Phrase(text));
        cell.setPadding(0);
        cell.setHorizontalAlignment(alignment);
        cell.setBorder(PdfPCell.NO_BORDER);
        return cell;
    }

    public PdfPCell getCell(Paragraph p, int alignment) {
        PdfPCell cell = new PdfPCell();
        cell.addElement(p);
        cell.setPadding(0);
        cell.setHorizontalAlignment(alignment);
        cell.setBorder(PdfPCell.NO_BORDER);
        return cell;
    }

    private void addRows(List<Pair<String, String>> rows, int type, Font font, PdfPTable table)
            throws MalformedURLException, IOException, DocumentException {
        for (Pair<String, String> row : rows) {
            PdfPCell cell;
            
            if (type == 0) {
            	Image image1 = Image.getInstance(row.getValue0());
                image1.scaleAbsolute(80, 60);
            	cell = new PdfPCell(image1);
            } else {
                String defaultFont = "src/main/resources/static/font/Roboto/Roboto-Regular.ttf";
                FontFactory.register(defaultFont, "defaultFont");
                BaseFont baseDefaultFont = BaseFont.createFont(defaultFont, BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
                Font secondBodyFont = new Font(baseDefaultFont, 12, Font.NORMAL, BaseColor.BLACK);
                Chunk rightSides = new Chunk(row.getValue0(), secondBodyFont);
                Paragraph text = new Paragraph(rightSides);
                cell = new PdfPCell(text);
            	// cell = new PdfPCell(new Phrase(row.getValue0()));
            }
            
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
//                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setPaddingBottom(10);
            cell.setBorder(Rectangle.NO_BORDER);
            table.addCell(cell);
            
            if (type == 0) {
            	cell = new PdfPCell(new Phrase(""));
            	cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            } else {
            	cell = new PdfPCell(new Phrase(":"));
            	cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            }
//                cell.setPaddingLeft(5);
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell.setPaddingBottom(10);
            cell.setBorder(Rectangle.NO_BORDER);
            table.addCell(cell);

            String defaultFont = "src/main/resources/static/font/Roboto/Roboto-Regular.ttf";
            FontFactory.register(defaultFont, "defaultFont");
            BaseFont baseDefaultFont = BaseFont.createFont(defaultFont, BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            Font secondBodyFont = new Font(baseDefaultFont, 12, Font.BOLD, BaseColor.BLACK);
            Chunk rightSides = new Chunk(row.getValue1(), secondBodyFont);
            Paragraph text = new Paragraph(rightSides);
            cell = new PdfPCell(text);
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            if (type == 0) {
            	cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            } else {
            	cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            }
            cell.setPaddingRight(5);
            cell.setPaddingBottom(10);
            cell.setBorder(Rectangle.NO_BORDER);
            table.addCell(cell);
        }
        

    }

    
}
