package pwc.generator.pdf.service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.html.WebColors;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import org.javatuples.Pair;

public class CetakanBillingGenerator {

    private PdfPTable table = new PdfPTable(3);
    private PdfPTable documentFooterTable = new PdfPTable(3);
    private PdfPTable noteTable = new PdfPTable(1);

    public ByteArrayInputStream generateCetakanKodeBilling() throws DocumentException, MalformedURLException, IOException {
        Document document = new Document(PageSize.A4);
        document.setMargins(36, 36, 36, 0);
        
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        PdfWriter.getInstance(document, output);

        document.open();

        table.setWidthPercentage(100);
        table.setWidths(new int[] { 4, 1, 10 });

        String defaultFont = "src/main/resources/static/font/Roboto/Roboto-Regular.ttf";
        FontFactory.register(defaultFont, "defaultFont");
        BaseFont baseDefaultFont = BaseFont.createFont(defaultFont, BaseFont.IDENTITY_H, BaseFont.EMBEDDED);

        // BaseColor
        BaseColor documentTitleBackColor = WebColors.getRGBColor("E03021");
        BaseColor documentHeaderFontColor = WebColors.getRGBColor("F16333");

        // Fonts
        Font documentTitleFont = new Font(baseDefaultFont, 12, Font.BOLD, BaseColor.WHITE);
        Font documentHeaderFont = new Font(baseDefaultFont, 18, Font.BOLD, documentHeaderFontColor);
        Font firstBodyFont1 = new Font(baseDefaultFont, 12, Font.NORMAL, BaseColor.BLACK);
        Font firstBodyFont2 = new Font(baseDefaultFont, 12, Font.BOLD, BaseColor.BLACK);
        Font secondBodyFont = new Font(baseDefaultFont, 14, Font.BOLD, BaseColor.BLACK);
        Font thirdBodyFont = new Font(baseDefaultFont, 12, Font.BOLD, documentTitleBackColor);
        Font normalNoteFont = new Font(baseDefaultFont, 9, Font.NORMAL, BaseColor.BLACK);
        Font boldNoteFont = new Font(baseDefaultFont, 9, Font.BOLD, documentTitleBackColor);
        Font italicNoteFont = new Font(baseDefaultFont, 9, Font.ITALIC, BaseColor.BLACK);

        /**
         * Document Title | "CETAKAN KODE BILLING"
         */
        Chunk documentTitleBackground = new Chunk("CETAKAN KODE BILLING", documentTitleFont);
        documentTitleBackground.setBackground(documentTitleBackColor, 5, 3, 5, 5);
        Paragraph documentTitleText = new Paragraph(documentTitleBackground);
        documentTitleText.setAlignment(PdfPCell.ALIGN_RIGHT);

        addRow(documentTitleText, 3);

        /**
         * Document Header | "ASP | PT PRIMA WAHANA CARAKA"
         */
        String pathHeaderAspLogo = "classpath:/static/icon/asp-logo.png";
        String pathHeaderSeparatorLogo = "classpath:/static/icon/header-separator.png";
        String headerAspName = "PT FINTEK INTEGRASI DIGITAL";

        // ASP LOGO
        Image headerAspLogo = Image.getInstance(pathHeaderAspLogo);
        PdfPCell headerAspLogoCell = new PdfPCell(headerAspLogo);
        headerAspLogoCell.setBorder(Rectangle.NO_BORDER);
        headerAspLogoCell.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);

        table.addCell(headerAspLogoCell);

        // HEADER LINE SEPARATOR
        Image headerSeparator = Image.getInstance(pathHeaderSeparatorLogo);
        PdfPCell headerSeparatorCell = new PdfPCell(headerSeparator);
        headerSeparatorCell.setBorder(Rectangle.NO_BORDER);
        headerSeparatorCell.setHorizontalAlignment(PdfPCell.ALIGN_MIDDLE);

        table.addCell(headerSeparatorCell);

        // COMPANY NAME
        Chunk chunkHeaderAspName = new Chunk(headerAspName, documentHeaderFont);
        Paragraph textHeaderAspName = new Paragraph(chunkHeaderAspName);
        textHeaderAspName.setAlignment(PdfPCell.ALIGN_LEFT);
        PdfPCell headerAspNameCell = new PdfPCell(textHeaderAspName);
        headerAspNameCell.setBorder(Rectangle.NO_BORDER);
        headerAspNameCell.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        headerAspNameCell.setVerticalAlignment(PdfPCell.ALIGN_MIDDLE);

        table.addCell(headerAspNameCell);

        // BREAK
        addRow(" ", firstBodyFont1, 3);

        /**
         * Document First Body
         */
        List<Pair<String, String>> firstBodyRows = new ArrayList<>();
        firstBodyRows.add(new Pair<>("NPWP", "31.400.108.2-121.100"));
        firstBodyRows.add(new Pair<>("NAMA", "Vladimir Kramnik"));
        firstBodyRows.add(new Pair<>("ALAMAT", "Jl. Lorem ipsum dolor sit amet, RT/RW 013/009, Kec. Lorem, Ipsum, 1234"));
        firstBodyRows.add(new Pair<>("NDP", "XXXXXXXXXXXXXXXX"));
        firstBodyRows.add(new Pair<>("JENIS PAJAK", "411211 - PPN Dalam Negeri"));
        firstBodyRows.add(new Pair<>("JENIS SETORAN", "102 - Setoran PPN JKP dari Luar Daerah Pabean"));
        firstBodyRows.add(new Pair<>("MASA PAJAK", "12"));
        firstBodyRows.add(new Pair<>("TAHUN PAJAK", "2020"));
        firstBodyRows.add(new Pair<>("NOMOR KETETAPAN", "XXXXXXXXXXXXXX"));
        firstBodyRows.add(new Pair<>("JUMLAH SETOR", "Rp 12.000"));
        firstBodyRows.add(new Pair<>("TERBILANG", "Duabelas ribu rupiah"));
        firstBodyRows.add(new Pair<>("Uraian", "-"));

        addRows(firstBodyRows, 1, firstBodyFont1, firstBodyFont2, table);

        // BREAK
        addRow(" ", firstBodyFont1, 3);

        List<Pair<String, String>> secondBodyRows = new ArrayList<>();
        secondBodyRows.add(new Pair<>("NPWP PENYETOR", "31.400.108.2-121.100"));
        secondBodyRows.add(new Pair<>("NAMA PENYETOR", "Vladimir Kramnik"));

        addRows(secondBodyRows, 1, firstBodyFont1, firstBodyFont2, table);

        // BREAK
        addRow(" ", firstBodyFont1, 3);

        addRow("GUNAKAN KODE BILLING DIBAWAH INI UNTUK MELAKUKAN PEMBAYARAN", firstBodyFont1, 3);
        List<Pair<String, String>> thirdBodyRows = new ArrayList<>();
        thirdBodyRows.add(new Pair<>("ID BILLING", "XXXXXXXXXXXXXXXXX"));
        thirdBodyRows.add(new Pair<>("MASA AKTIF", "01/12/2020"));

        addRows(thirdBodyRows, 1, thirdBodyFont, firstBodyFont2, table);

        document.add(table);

        /**
         *  Document Footer
         */
        documentFooterTable.setSpacingBefore(67);
        documentFooterTable.setWidthPercentage(100);
        documentFooterTable.setWidths(new int[] {6, 5, 1});
        String pathDjpBarcode = "classpath:/static/icon/djp-barcode.png";

        Chunk chunkHalaman = new Chunk("Halaman 1 dari 1", firstBodyFont1);
        Paragraph textHalaman = new Paragraph(chunkHalaman);
        PdfPCell halamanCell = new PdfPCell();
        halamanCell.addElement(textHalaman);
        halamanCell.setBorder(Rectangle.NO_BORDER);
        halamanCell.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        halamanCell.setVerticalAlignment(PdfPCell.ALIGN_BOTTOM);

        documentFooterTable.addCell(halamanCell);

        Chunk chunkTerdaftar = new Chunk("TERDAFTAR DAN DIAWASI OLEH DJP", firstBodyFont1);
        Paragraph textTerdaftar = new Paragraph(chunkTerdaftar);
        PdfPCell terdaftarCell = new PdfPCell();
        terdaftarCell.addElement(textTerdaftar);
        terdaftarCell.setBorder(Rectangle.NO_BORDER);
        terdaftarCell.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
        terdaftarCell.setVerticalAlignment(PdfPCell.ALIGN_BOTTOM);

        documentFooterTable.addCell(terdaftarCell);

        Image djpBarcodeImage = Image.getInstance(pathDjpBarcode);
        PdfPCell djpBarcodeCell = new PdfPCell(djpBarcodeImage);
        djpBarcodeCell.setBorder(Rectangle.NO_BORDER);
        djpBarcodeCell.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        djpBarcodeCell.setVerticalAlignment(PdfPCell.ALIGN_MIDDLE);

        documentFooterTable.addCell(djpBarcodeCell);

        document.add(documentFooterTable);

        /**
         * Catatan
         */
        noteTable.setSpacingBefore(5);
        noteTable.setWidthPercentage(113);

        Chunk chunkFirstNote = new Chunk("Catatan: Apabila ada kesalahan dalam isian Kode Billing atau masa berlakunya berakhir, Kode", italicNoteFont);
        Paragraph textFirstNote = new Paragraph(chunkFirstNote);
        textFirstNote.setAlignment(PdfPCell.ALIGN_CENTER);

        Chunk chunkSecondNote = new Chunk("Billing dapat dibuat kembali. Tanggung jawab isian Kode Billing ada pada Wajib Pajak yang", italicNoteFont);
        Paragraph textSecondNote = new Paragraph(chunkSecondNote);
        textSecondNote.setAlignment(PdfPCell.ALIGN_CENTER);

        Chunk chunkThirdNote = new Chunk("namanya tercantum di dalamnya.", italicNoteFont);
        Paragraph textThirdNote = new Paragraph(chunkThirdNote);
        textThirdNote.setAlignment(PdfPCell.ALIGN_CENTER);
        
        PdfPCell noteCell = new PdfPCell();
        noteCell.addElement(textFirstNote);
        noteCell.addElement(textSecondNote);
        noteCell.addElement(textThirdNote);
        noteCell.setPadding(15);

        noteCell.setBorder(Rectangle.NO_BORDER);
        noteCell.setBackgroundColor(new BaseColor(252,229,224));

        noteTable.addCell(noteCell);

        document.add(noteTable);

        document.close();
        return new ByteArrayInputStream(output.toByteArray());
    }

    private void addRow(String s, Font font, int colspan) {
        PdfPCell pdfPCell = new PdfPCell(new Phrase(s, font));
        pdfPCell.setColspan(colspan);
        pdfPCell.setPaddingBottom(10); // NEW
        pdfPCell.setBorder(Rectangle.NO_BORDER);
        table.addCell(pdfPCell);
    }

    private void addRow(Paragraph p, int colspan) {
        PdfPCell cell = new PdfPCell();
        cell.addElement(p);
        cell.setColspan(colspan);
        cell.setPaddingBottom(10);
        cell.setBorder(Rectangle.NO_BORDER);
        table.addCell(cell);
    }

    private void addRows(List<Pair<String, String>> rows, int type, Font font1, Font font2, PdfPTable table)
            throws MalformedURLException, IOException, DocumentException {

        if (type == 0) {
            table.setWidths(new int[] {5, 1, 5});
        }

        for (Pair<String, String> row : rows) {
            PdfPCell cell;
            
            Chunk rightSides = new Chunk(row.getValue0(), font1);
            Paragraph text = new Paragraph(rightSides);
            cell = new PdfPCell(text);
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell.setPaddingBottom(10);
            cell.setBorder(Rectangle.NO_BORDER);
            table.addCell(cell);
            
            cell = new PdfPCell(new Phrase(":"));
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell.setPaddingBottom(10);
            cell.setBorder(Rectangle.NO_BORDER);
            table.addCell(cell);

            Chunk rightSides2 = new Chunk(row.getValue1(), font2);
            Paragraph text2 = new Paragraph(rightSides2);
            cell = new PdfPCell(text2);
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);    
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setPaddingBottom(10);
            cell.setBorder(Rectangle.NO_BORDER);
            table.addCell(cell);
        }
    }
    
}
