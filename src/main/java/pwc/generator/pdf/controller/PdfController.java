package pwc.generator.pdf.controller;

import java.io.ByteArrayInputStream;
import java.io.IOException;

import com.itextpdf.text.DocumentException;

import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pwc.generator.pdf.service.BpaGenerator;
import pwc.generator.pdf.service.BpeGenerator;
import pwc.generator.pdf.service.CetakanBillingGenerator;

@RestController
@RequestMapping(value = "/pdf")
public class PdfController {

    @GetMapping(value = "/bpa", produces = MediaType.APPLICATION_PDF_VALUE)
    public ResponseEntity<InputStreamResource> generateBpa() throws DocumentException, IOException {
        BpaGenerator bpaGenerator = new BpaGenerator();
        ByteArrayInputStream bpaStream = bpaGenerator.generateBpa();

        HttpHeaders headers = new HttpHeaders();

        // SET FILENAME
        String filename = "test-bpa-1";

        // inline: show on tab, attachment: download directly
        headers.add("Content-Disposition", "inline; filename=\"" + filename + ".pdf\"");
        
        InputStreamResource bpa = new InputStreamResource(bpaStream);

        return ResponseEntity
            .ok()
            .headers(headers)
            .contentType(MediaType.APPLICATION_PDF)
            .body(bpa);
    }

    @GetMapping(value = "/bpe", produces = MediaType.APPLICATION_PDF_VALUE)
    public ResponseEntity<InputStreamResource> generateBpe() throws DocumentException, IOException {
        BpeGenerator bpaGenerator = new BpeGenerator();
        ByteArrayInputStream bpaStream = bpaGenerator.generateBpe();

        HttpHeaders headers = new HttpHeaders();

        // SET FILENAME
        String filename = "test-bpe-1";

        // inline: show on tab, attachment: download directly
        headers.add("Content-Disposition", "inline; filename=\"" + filename + ".pdf\"");
        
        InputStreamResource bpa = new InputStreamResource(bpaStream);

        return ResponseEntity
            .ok()
            .headers(headers)
            .contentType(MediaType.APPLICATION_PDF)
            .body(bpa);
    }

    @GetMapping(value = "/cetakanBilling", produces = MediaType.APPLICATION_PDF_VALUE)
    public ResponseEntity<InputStreamResource> generateCetakanKodeBilling() throws DocumentException, IOException {
        CetakanBillingGenerator cetakanBillingGenerator = new CetakanBillingGenerator();
        ByteArrayInputStream cetakanBillingStream = cetakanBillingGenerator.generateCetakanKodeBilling();

        HttpHeaders headers = new HttpHeaders();

        // SET FILENAME
        String filename = "test-cetakan-billing-1";

        // inline: show on tab, attachment: download directly
        headers.add("Content-Disposition", "inline; filename=\"" + filename + ".pdf\"");
        
        InputStreamResource bpa = new InputStreamResource(cetakanBillingStream);

        return ResponseEntity
            .ok()
            .headers(headers)
            .contentType(MediaType.APPLICATION_PDF)
            .body(bpa);
    }
    
}
